import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';

/**
 * `grid-arr-list`
 * Grid Arrangement List
 * 
 * Para:
 *  items array -- a array to keep objects list which object structure can be applicaiton defined. 
 *
 * @customElement
 * @polymer
 * @demo demo/index.html
 */
class GridArrList extends PolymerElement {
  static get template() {
    return html`
      <style>
        :host {
          display: block;
        }

        .grid {
          @apply --layout-horizontal;
          @apply --layout-center-center;
          @apply --layout-wrap;
          margin-top: 10px;
        }
  
        .grid > .item {
          -webkit-flex: 1 calc(33% - 20px);
          flex: 1 calc(33% - 20px);
          max-width: calc(33% - 20px);
          /* height: 30vmax; */
          min-height: 250px;
          margin: 10px;
          text-decoration: none;
          background-color: white;
        }
  
        /**
         * Desktop small, tablet
         */
        @media (max-width: 1200px) {
          .grid {
            padding-left: 10px;
            padding-right: 10px;
            font-size: 0.7em;
          }
  
          .grid > .item {
            -webkit-flex: 1 calc(50% - 20px);
            flex: 1 calc(50% - 20px);
            max-width: calc(50% - 20px);
            margin: 10px;
            text-decoration: none;
            background-color: white;
          }
        }
  
        /**
         * Phone
         */
        @media (max-width: 400px) {
          .grid {
            padding-left: 10px;
            padding-right: 10px;
          }
  
          .grid > .item {
            -webkit-flex: 1 calc(100% - 10px);
            flex: 1 calc(1000% - 10px);
            max-width: calc(100% - 10px);
            margin: 5px;
            text-decoration: none;
            background-color: white;
          }
        }

        </style>

      <div class="grid">
        <template is="dom-repeat" items="[[items]]" index-as="id">
          <div class="item">
            <slot name="grid-arr-list-[[id]]">No Inserting</slot>
          </div>
        </template>
        <!-- sort="_sortItems" -->
      </div>
    `;
  }
  static get properties() {
    return {

      // default items for demo display
      items: {
        type: Array,
        value: () => [
          {"demo1":"demo1.1",
            "demo2":"demo2.1",
          },
          {"demo1":"demo1.2",
            "demo2":"demo2.2",
          },
          {"demo1":"demo1.3",
            "demo2":"demo2.3",
          },
          {"demo1":"demo1.4",
            "demo2":"demo2.4",
          },
          {"demo1":"demo1.5",
            "demo2":"demo2.5",
          },
        ]
      },

    };
  }

  /*_sortItems() {
    return Math.round(Math.random() * 3) - 1;
  }*/

}

window.customElements.define('grid-arr-list', GridArrList);
